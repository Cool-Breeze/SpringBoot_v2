package com.fc.v2.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: 清风
 * @create: 2023年03月09日 08:11
 * @program: CSGO_manage
 * @description:
 **/
public class HttpUtils {


    public static void main(String[] args) {
        String url = "https://csgoob.com/api/v1/goods/info";

        // post请求
        HttpClient httpClient = null;
        HttpPost postMethod = null;
        HttpResponse response = null;
        String responseContent = null;

        try {
            // 获取http客户端
            httpClient = HttpClients.createDefault();
            postMethod = new HttpPost(url);

            // 设置请求头
            postMethod.addHeader("Content-Type", "application/json;charset=utf8");
            // 封装请求体
            Map<String, String> map = new HashMap<>();
            map.put("goodsName", "M4A1 消音型 | 印花集 (久经沙场)");
            postMethod.setEntity(new StringEntity(JSON.toJSONString(map), Charset.forName("UTF-8")));
            // 发送请求
            response = httpClient.execute(postMethod);

            // 获取响应结果
            int statusCode = response.getStatusLine().getStatusCode();

            // 响应对象
            HttpEntity httpEntity = response.getEntity();

            // 响应的字符串
            responseContent = EntityUtils.toString(httpEntity, "UTF-8");

            JSONObject jsonObject = JSON.parseObject(responseContent);
            System.out.println("responseContent:" + jsonObject.get("data"));
            //释放资源
            EntityUtils.consume(httpEntity);
            return;
        } catch (IOException e) {
            System.out.println("请求异常, 错误信息为: {} " + e.getMessage());
            return;
        }
    }
}
